package com.upplication.training.sql;


import com.upplication.training.sql.domain.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import javax.persistence.EntityManager;

import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.assertEquals;


/**
 * Briefing
 * --------
 * We are under a heavy bruteforce attack and we are implementing a *slowban algorithm so we are
 * checking the number of wrong logins to track the users being attacked. We need you to find those
 * users based on failed attempts in the last week (7 days), later we will implement some rules once
 * targets have been identified.
 *
 * * A slowbanned user or ip has delays forcibly introduced into every page they visit.
 * From their perspective, your site has just gotten terribly, horribly slow. And stays that way.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.HSQL)
@Sql("classpath:usr.sql")
public class SqlTest {

    private Calendar today = Calendar.getInstance();

    @Autowired
    private EntityManager entityManager;

    @Before
    public void setup() {
        today.set(Calendar.YEAR, 2017);
        today.set(Calendar.MONTH, Calendar.MAY);
        today.set(Calendar.DAY_OF_MONTH, 30);
    }

    @Test
    public void example() {
        List<User> users = entityManager.createQuery("FROM User", User.class).getResultList();
        assertEquals(12, users.size());
    }

    /**
     * Implement the query with the entityManager
     * and show the results in the console.
     *
     * We don't want a real implementation.
     *
     * Use the EntityManager and create a valid query that give us
     * the username and the failed attempts in the last 7 days.
     */
    @Test
    public void failed_attempts_in_the_last_7_days_from_2017_MAY_30() {

    }
}
