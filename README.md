## Training Backend Upplication

Welcome! This is an exercise for [backend developers](http://backend.upplication.com)
We hope you have fun and do your best :)

### How its work?

1. Fork this repo
2. Apply your solution
3. Review and... review again. Try to keep special attention on details.
4. Make a [Patch](http://www.thegeekstuff.com/2014/03/git-patch-create-and-apply) with your changes and send to us with a
clear description
5. Wait for our response :)

Thanks for taking the time to take this little test with us.

### About this exercise

We are under a heavy bruteforce attack and we are implementing a *slowban algorithm so we are
checking the number of wrong logins to track the users being attacked. We need you to find those
users based on failed attempts in the last week (7 days), later we will implement some rules once
targets have been identified.

* A slowbanned user or ip has delays forcibly introduced into every page they visit.
From their perspective, your site has just gotten terribly, horribly slow. And stays that way.

*Example 'users' table*

| id | name | designation | status |
| --- | --- | --- | --- |
| 1 | Justiño Palmeiro | HIPERADMIN | 1 |
| 2 | Dani Twister | MEGAADMIN | 0 |
| 3 | Marty McFly | TIME_TRAVELER | 1 |
| ... | ... | ... | ... |

*Example 'logins' table*

| id | user | success | day |
| --- | --- | --- | --- |
| 1 | 1 | 1 | 2017-10-21 00:00:00 |
| 2 | 2 | 0 | 2017-10-21 00:00:00 |
| ... | ... | ... | ... |

*Example output*

Note that 'Dani Twister' is missing because its a disabled user

| username | failed_count |
| --- | --- | --- | --- |
| Justiño Palmeiro | 20000 |
| Marty McFly | 2 |
| ... | ... |

We have test in `com.upplication.training.sql.SqlTest` where you can test and apply the solution. Read the Javadoc
and the comments.

You must implement the test: `failed_attempts_in_the_last_7_days_from_2017_MAY_30` and print in the console the results
like the 'example output' section.

You can use `HQL` or `SQL` with the [EntityManager](http://docs.oracle.com/javaee/7/api/javax/persistence/EntityManager.html)

Apply your solution in about ~ 15 min.